package webadv.s16202126.Governmentweb.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import webadv.s16202126.Governmentweb.interceptor.LoginInterceptor;

@Configuration
public class LoginConfig implements WebMvcConfigurer {
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoginInterceptor()).excludePathPatterns("/**","/login","/css/**","/js/**","/vendors/**","/fonts/**","/images/**");
	}
}
