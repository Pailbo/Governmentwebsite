package webadv.s16202126.Governmentweb.entity;

import java.util.ArrayList;
import java.util.List;

public class GoOnExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public GoOnExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGoIdIsNull() {
            addCriterion("go_id is null");
            return (Criteria) this;
        }

        public Criteria andGoIdIsNotNull() {
            addCriterion("go_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoIdEqualTo(Integer value) {
            addCriterion("go_id =", value, "goId");
            return (Criteria) this;
        }

        public Criteria andGoIdNotEqualTo(Integer value) {
            addCriterion("go_id <>", value, "goId");
            return (Criteria) this;
        }

        public Criteria andGoIdGreaterThan(Integer value) {
            addCriterion("go_id >", value, "goId");
            return (Criteria) this;
        }

        public Criteria andGoIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("go_id >=", value, "goId");
            return (Criteria) this;
        }

        public Criteria andGoIdLessThan(Integer value) {
            addCriterion("go_id <", value, "goId");
            return (Criteria) this;
        }

        public Criteria andGoIdLessThanOrEqualTo(Integer value) {
            addCriterion("go_id <=", value, "goId");
            return (Criteria) this;
        }

        public Criteria andGoIdIn(List<Integer> values) {
            addCriterion("go_id in", values, "goId");
            return (Criteria) this;
        }

        public Criteria andGoIdNotIn(List<Integer> values) {
            addCriterion("go_id not in", values, "goId");
            return (Criteria) this;
        }

        public Criteria andGoIdBetween(Integer value1, Integer value2) {
            addCriterion("go_id between", value1, value2, "goId");
            return (Criteria) this;
        }

        public Criteria andGoIdNotBetween(Integer value1, Integer value2) {
            addCriterion("go_id not between", value1, value2, "goId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNull() {
            addCriterion("home_id is null");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNotNull() {
            addCriterion("home_id is not null");
            return (Criteria) this;
        }

        public Criteria andHomeIdEqualTo(Long value) {
            addCriterion("home_id =", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotEqualTo(Long value) {
            addCriterion("home_id <>", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThan(Long value) {
            addCriterion("home_id >", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("home_id >=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThan(Long value) {
            addCriterion("home_id <", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThanOrEqualTo(Long value) {
            addCriterion("home_id <=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIn(List<Long> values) {
            addCriterion("home_id in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotIn(List<Long> values) {
            addCriterion("home_id not in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdBetween(Long value1, Long value2) {
            addCriterion("home_id between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotBetween(Long value1, Long value2) {
            addCriterion("home_id not between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andGoNaneIsNull() {
            addCriterion("go_nane is null");
            return (Criteria) this;
        }

        public Criteria andGoNaneIsNotNull() {
            addCriterion("go_nane is not null");
            return (Criteria) this;
        }

        public Criteria andGoNaneEqualTo(String value) {
            addCriterion("go_nane =", value, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneNotEqualTo(String value) {
            addCriterion("go_nane <>", value, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneGreaterThan(String value) {
            addCriterion("go_nane >", value, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneGreaterThanOrEqualTo(String value) {
            addCriterion("go_nane >=", value, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneLessThan(String value) {
            addCriterion("go_nane <", value, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneLessThanOrEqualTo(String value) {
            addCriterion("go_nane <=", value, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneLike(String value) {
            addCriterion("go_nane like", value, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneNotLike(String value) {
            addCriterion("go_nane not like", value, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneIn(List<String> values) {
            addCriterion("go_nane in", values, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneNotIn(List<String> values) {
            addCriterion("go_nane not in", values, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneBetween(String value1, String value2) {
            addCriterion("go_nane between", value1, value2, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoNaneNotBetween(String value1, String value2) {
            addCriterion("go_nane not between", value1, value2, "goNane");
            return (Criteria) this;
        }

        public Criteria andGoLinkIsNull() {
            addCriterion("go_link is null");
            return (Criteria) this;
        }

        public Criteria andGoLinkIsNotNull() {
            addCriterion("go_link is not null");
            return (Criteria) this;
        }

        public Criteria andGoLinkEqualTo(String value) {
            addCriterion("go_link =", value, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkNotEqualTo(String value) {
            addCriterion("go_link <>", value, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkGreaterThan(String value) {
            addCriterion("go_link >", value, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkGreaterThanOrEqualTo(String value) {
            addCriterion("go_link >=", value, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkLessThan(String value) {
            addCriterion("go_link <", value, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkLessThanOrEqualTo(String value) {
            addCriterion("go_link <=", value, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkLike(String value) {
            addCriterion("go_link like", value, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkNotLike(String value) {
            addCriterion("go_link not like", value, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkIn(List<String> values) {
            addCriterion("go_link in", values, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkNotIn(List<String> values) {
            addCriterion("go_link not in", values, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkBetween(String value1, String value2) {
            addCriterion("go_link between", value1, value2, "goLink");
            return (Criteria) this;
        }

        public Criteria andGoLinkNotBetween(String value1, String value2) {
            addCriterion("go_link not between", value1, value2, "goLink");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}