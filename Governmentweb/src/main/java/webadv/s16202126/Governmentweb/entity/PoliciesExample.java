package webadv.s16202126.Governmentweb.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PoliciesExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public PoliciesExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPoliciesIdIsNull() {
            addCriterion("policies_id is null");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdIsNotNull() {
            addCriterion("policies_id is not null");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdEqualTo(Integer value) {
            addCriterion("policies_id =", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdNotEqualTo(Integer value) {
            addCriterion("policies_id <>", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdGreaterThan(Integer value) {
            addCriterion("policies_id >", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("policies_id >=", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdLessThan(Integer value) {
            addCriterion("policies_id <", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdLessThanOrEqualTo(Integer value) {
            addCriterion("policies_id <=", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdIn(List<Integer> values) {
            addCriterion("policies_id in", values, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdNotIn(List<Integer> values) {
            addCriterion("policies_id not in", values, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdBetween(Integer value1, Integer value2) {
            addCriterion("policies_id between", value1, value2, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdNotBetween(Integer value1, Integer value2) {
            addCriterion("policies_id not between", value1, value2, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleIsNull() {
            addCriterion("policies_title is null");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleIsNotNull() {
            addCriterion("policies_title is not null");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleEqualTo(String value) {
            addCriterion("policies_title =", value, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleNotEqualTo(String value) {
            addCriterion("policies_title <>", value, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleGreaterThan(String value) {
            addCriterion("policies_title >", value, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleGreaterThanOrEqualTo(String value) {
            addCriterion("policies_title >=", value, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleLessThan(String value) {
            addCriterion("policies_title <", value, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleLessThanOrEqualTo(String value) {
            addCriterion("policies_title <=", value, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleLike(String value) {
            addCriterion("policies_title like", value, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleNotLike(String value) {
            addCriterion("policies_title not like", value, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleIn(List<String> values) {
            addCriterion("policies_title in", values, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleNotIn(List<String> values) {
            addCriterion("policies_title not in", values, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleBetween(String value1, String value2) {
            addCriterion("policies_title between", value1, value2, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTitleNotBetween(String value1, String value2) {
            addCriterion("policies_title not between", value1, value2, "policiesTitle");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeIsNull() {
            addCriterion("policies_time is null");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeIsNotNull() {
            addCriterion("policies_time is not null");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeEqualTo(Date value) {
            addCriterion("policies_time =", value, "policiesTime");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeNotEqualTo(Date value) {
            addCriterion("policies_time <>", value, "policiesTime");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeGreaterThan(Date value) {
            addCriterion("policies_time >", value, "policiesTime");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("policies_time >=", value, "policiesTime");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeLessThan(Date value) {
            addCriterion("policies_time <", value, "policiesTime");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeLessThanOrEqualTo(Date value) {
            addCriterion("policies_time <=", value, "policiesTime");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeIn(List<Date> values) {
            addCriterion("policies_time in", values, "policiesTime");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeNotIn(List<Date> values) {
            addCriterion("policies_time not in", values, "policiesTime");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeBetween(Date value1, Date value2) {
            addCriterion("policies_time between", value1, value2, "policiesTime");
            return (Criteria) this;
        }

        public Criteria andPoliciesTimeNotBetween(Date value1, Date value2) {
            addCriterion("policies_time not between", value1, value2, "policiesTime");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}