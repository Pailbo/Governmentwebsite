package webadv.s16202126.Governmentweb.entity;

import java.util.ArrayList;
import java.util.List;

public class LawsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public LawsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLawIdIsNull() {
            addCriterion("law_id is null");
            return (Criteria) this;
        }

        public Criteria andLawIdIsNotNull() {
            addCriterion("law_id is not null");
            return (Criteria) this;
        }

        public Criteria andLawIdEqualTo(Integer value) {
            addCriterion("law_id =", value, "lawId");
            return (Criteria) this;
        }

        public Criteria andLawIdNotEqualTo(Integer value) {
            addCriterion("law_id <>", value, "lawId");
            return (Criteria) this;
        }

        public Criteria andLawIdGreaterThan(Integer value) {
            addCriterion("law_id >", value, "lawId");
            return (Criteria) this;
        }

        public Criteria andLawIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("law_id >=", value, "lawId");
            return (Criteria) this;
        }

        public Criteria andLawIdLessThan(Integer value) {
            addCriterion("law_id <", value, "lawId");
            return (Criteria) this;
        }

        public Criteria andLawIdLessThanOrEqualTo(Integer value) {
            addCriterion("law_id <=", value, "lawId");
            return (Criteria) this;
        }

        public Criteria andLawIdIn(List<Integer> values) {
            addCriterion("law_id in", values, "lawId");
            return (Criteria) this;
        }

        public Criteria andLawIdNotIn(List<Integer> values) {
            addCriterion("law_id not in", values, "lawId");
            return (Criteria) this;
        }

        public Criteria andLawIdBetween(Integer value1, Integer value2) {
            addCriterion("law_id between", value1, value2, "lawId");
            return (Criteria) this;
        }

        public Criteria andLawIdNotBetween(Integer value1, Integer value2) {
            addCriterion("law_id not between", value1, value2, "lawId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdIsNull() {
            addCriterion("investment_id is null");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdIsNotNull() {
            addCriterion("investment_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdEqualTo(Integer value) {
            addCriterion("investment_id =", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdNotEqualTo(Integer value) {
            addCriterion("investment_id <>", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdGreaterThan(Integer value) {
            addCriterion("investment_id >", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("investment_id >=", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdLessThan(Integer value) {
            addCriterion("investment_id <", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdLessThanOrEqualTo(Integer value) {
            addCriterion("investment_id <=", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdIn(List<Integer> values) {
            addCriterion("investment_id in", values, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdNotIn(List<Integer> values) {
            addCriterion("investment_id not in", values, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdBetween(Integer value1, Integer value2) {
            addCriterion("investment_id between", value1, value2, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("investment_id not between", value1, value2, "investmentId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdIsNull() {
            addCriterion("policies_id is null");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdIsNotNull() {
            addCriterion("policies_id is not null");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdEqualTo(Integer value) {
            addCriterion("policies_id =", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdNotEqualTo(Integer value) {
            addCriterion("policies_id <>", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdGreaterThan(Integer value) {
            addCriterion("policies_id >", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("policies_id >=", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdLessThan(Integer value) {
            addCriterion("policies_id <", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdLessThanOrEqualTo(Integer value) {
            addCriterion("policies_id <=", value, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdIn(List<Integer> values) {
            addCriterion("policies_id in", values, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdNotIn(List<Integer> values) {
            addCriterion("policies_id not in", values, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdBetween(Integer value1, Integer value2) {
            addCriterion("policies_id between", value1, value2, "policiesId");
            return (Criteria) this;
        }

        public Criteria andPoliciesIdNotBetween(Integer value1, Integer value2) {
            addCriterion("policies_id not between", value1, value2, "policiesId");
            return (Criteria) this;
        }

        public Criteria andLawTitleIsNull() {
            addCriterion("law_title is null");
            return (Criteria) this;
        }

        public Criteria andLawTitleIsNotNull() {
            addCriterion("law_title is not null");
            return (Criteria) this;
        }

        public Criteria andLawTitleEqualTo(String value) {
            addCriterion("law_title =", value, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleNotEqualTo(String value) {
            addCriterion("law_title <>", value, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleGreaterThan(String value) {
            addCriterion("law_title >", value, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleGreaterThanOrEqualTo(String value) {
            addCriterion("law_title >=", value, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleLessThan(String value) {
            addCriterion("law_title <", value, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleLessThanOrEqualTo(String value) {
            addCriterion("law_title <=", value, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleLike(String value) {
            addCriterion("law_title like", value, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleNotLike(String value) {
            addCriterion("law_title not like", value, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleIn(List<String> values) {
            addCriterion("law_title in", values, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleNotIn(List<String> values) {
            addCriterion("law_title not in", values, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleBetween(String value1, String value2) {
            addCriterion("law_title between", value1, value2, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawTitleNotBetween(String value1, String value2) {
            addCriterion("law_title not between", value1, value2, "lawTitle");
            return (Criteria) this;
        }

        public Criteria andLawContentIsNull() {
            addCriterion("law_content is null");
            return (Criteria) this;
        }

        public Criteria andLawContentIsNotNull() {
            addCriterion("law_content is not null");
            return (Criteria) this;
        }

        public Criteria andLawContentEqualTo(String value) {
            addCriterion("law_content =", value, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentNotEqualTo(String value) {
            addCriterion("law_content <>", value, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentGreaterThan(String value) {
            addCriterion("law_content >", value, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentGreaterThanOrEqualTo(String value) {
            addCriterion("law_content >=", value, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentLessThan(String value) {
            addCriterion("law_content <", value, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentLessThanOrEqualTo(String value) {
            addCriterion("law_content <=", value, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentLike(String value) {
            addCriterion("law_content like", value, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentNotLike(String value) {
            addCriterion("law_content not like", value, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentIn(List<String> values) {
            addCriterion("law_content in", values, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentNotIn(List<String> values) {
            addCriterion("law_content not in", values, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentBetween(String value1, String value2) {
            addCriterion("law_content between", value1, value2, "lawContent");
            return (Criteria) this;
        }

        public Criteria andLawContentNotBetween(String value1, String value2) {
            addCriterion("law_content not between", value1, value2, "lawContent");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}