package webadv.s16202126.Governmentweb.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VidioExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public VidioExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andVidioIdIsNull() {
            addCriterion("vidio_id is null");
            return (Criteria) this;
        }

        public Criteria andVidioIdIsNotNull() {
            addCriterion("vidio_id is not null");
            return (Criteria) this;
        }

        public Criteria andVidioIdEqualTo(Integer value) {
            addCriterion("vidio_id =", value, "vidioId");
            return (Criteria) this;
        }

        public Criteria andVidioIdNotEqualTo(Integer value) {
            addCriterion("vidio_id <>", value, "vidioId");
            return (Criteria) this;
        }

        public Criteria andVidioIdGreaterThan(Integer value) {
            addCriterion("vidio_id >", value, "vidioId");
            return (Criteria) this;
        }

        public Criteria andVidioIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("vidio_id >=", value, "vidioId");
            return (Criteria) this;
        }

        public Criteria andVidioIdLessThan(Integer value) {
            addCriterion("vidio_id <", value, "vidioId");
            return (Criteria) this;
        }

        public Criteria andVidioIdLessThanOrEqualTo(Integer value) {
            addCriterion("vidio_id <=", value, "vidioId");
            return (Criteria) this;
        }

        public Criteria andVidioIdIn(List<Integer> values) {
            addCriterion("vidio_id in", values, "vidioId");
            return (Criteria) this;
        }

        public Criteria andVidioIdNotIn(List<Integer> values) {
            addCriterion("vidio_id not in", values, "vidioId");
            return (Criteria) this;
        }

        public Criteria andVidioIdBetween(Integer value1, Integer value2) {
            addCriterion("vidio_id between", value1, value2, "vidioId");
            return (Criteria) this;
        }

        public Criteria andVidioIdNotBetween(Integer value1, Integer value2) {
            addCriterion("vidio_id not between", value1, value2, "vidioId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNull() {
            addCriterion("home_id is null");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNotNull() {
            addCriterion("home_id is not null");
            return (Criteria) this;
        }

        public Criteria andHomeIdEqualTo(Integer value) {
            addCriterion("home_id =", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotEqualTo(Integer value) {
            addCriterion("home_id <>", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThan(Integer value) {
            addCriterion("home_id >", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("home_id >=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThan(Integer value) {
            addCriterion("home_id <", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThanOrEqualTo(Integer value) {
            addCriterion("home_id <=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIn(List<Integer> values) {
            addCriterion("home_id in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotIn(List<Integer> values) {
            addCriterion("home_id not in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdBetween(Integer value1, Integer value2) {
            addCriterion("home_id between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("home_id not between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andVidioNameIsNull() {
            addCriterion("vidio_name is null");
            return (Criteria) this;
        }

        public Criteria andVidioNameIsNotNull() {
            addCriterion("vidio_name is not null");
            return (Criteria) this;
        }

        public Criteria andVidioNameEqualTo(String value) {
            addCriterion("vidio_name =", value, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameNotEqualTo(String value) {
            addCriterion("vidio_name <>", value, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameGreaterThan(String value) {
            addCriterion("vidio_name >", value, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameGreaterThanOrEqualTo(String value) {
            addCriterion("vidio_name >=", value, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameLessThan(String value) {
            addCriterion("vidio_name <", value, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameLessThanOrEqualTo(String value) {
            addCriterion("vidio_name <=", value, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameLike(String value) {
            addCriterion("vidio_name like", value, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameNotLike(String value) {
            addCriterion("vidio_name not like", value, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameIn(List<String> values) {
            addCriterion("vidio_name in", values, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameNotIn(List<String> values) {
            addCriterion("vidio_name not in", values, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameBetween(String value1, String value2) {
            addCriterion("vidio_name between", value1, value2, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioNameNotBetween(String value1, String value2) {
            addCriterion("vidio_name not between", value1, value2, "vidioName");
            return (Criteria) this;
        }

        public Criteria andVidioLinkIsNull() {
            addCriterion("vidio_link is null");
            return (Criteria) this;
        }

        public Criteria andVidioLinkIsNotNull() {
            addCriterion("vidio_link is not null");
            return (Criteria) this;
        }

        public Criteria andVidioLinkEqualTo(String value) {
            addCriterion("vidio_link =", value, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkNotEqualTo(String value) {
            addCriterion("vidio_link <>", value, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkGreaterThan(String value) {
            addCriterion("vidio_link >", value, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkGreaterThanOrEqualTo(String value) {
            addCriterion("vidio_link >=", value, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkLessThan(String value) {
            addCriterion("vidio_link <", value, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkLessThanOrEqualTo(String value) {
            addCriterion("vidio_link <=", value, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkLike(String value) {
            addCriterion("vidio_link like", value, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkNotLike(String value) {
            addCriterion("vidio_link not like", value, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkIn(List<String> values) {
            addCriterion("vidio_link in", values, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkNotIn(List<String> values) {
            addCriterion("vidio_link not in", values, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkBetween(String value1, String value2) {
            addCriterion("vidio_link between", value1, value2, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioLinkNotBetween(String value1, String value2) {
            addCriterion("vidio_link not between", value1, value2, "vidioLink");
            return (Criteria) this;
        }

        public Criteria andVidioTimeIsNull() {
            addCriterion("vidio_time is null");
            return (Criteria) this;
        }

        public Criteria andVidioTimeIsNotNull() {
            addCriterion("vidio_time is not null");
            return (Criteria) this;
        }

        public Criteria andVidioTimeEqualTo(Date value) {
            addCriterion("vidio_time =", value, "vidioTime");
            return (Criteria) this;
        }

        public Criteria andVidioTimeNotEqualTo(Date value) {
            addCriterion("vidio_time <>", value, "vidioTime");
            return (Criteria) this;
        }

        public Criteria andVidioTimeGreaterThan(Date value) {
            addCriterion("vidio_time >", value, "vidioTime");
            return (Criteria) this;
        }

        public Criteria andVidioTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("vidio_time >=", value, "vidioTime");
            return (Criteria) this;
        }

        public Criteria andVidioTimeLessThan(Date value) {
            addCriterion("vidio_time <", value, "vidioTime");
            return (Criteria) this;
        }

        public Criteria andVidioTimeLessThanOrEqualTo(Date value) {
            addCriterion("vidio_time <=", value, "vidioTime");
            return (Criteria) this;
        }

        public Criteria andVidioTimeIn(List<Date> values) {
            addCriterion("vidio_time in", values, "vidioTime");
            return (Criteria) this;
        }

        public Criteria andVidioTimeNotIn(List<Date> values) {
            addCriterion("vidio_time not in", values, "vidioTime");
            return (Criteria) this;
        }

        public Criteria andVidioTimeBetween(Date value1, Date value2) {
            addCriterion("vidio_time between", value1, value2, "vidioTime");
            return (Criteria) this;
        }

        public Criteria andVidioTimeNotBetween(Date value1, Date value2) {
            addCriterion("vidio_time not between", value1, value2, "vidioTime");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}