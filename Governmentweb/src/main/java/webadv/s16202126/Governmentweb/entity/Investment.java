package webadv.s16202126.Governmentweb.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * investment
 * @author 
 */
public class Investment implements Serializable {
    private Integer investmentId;

    private Integer homeId;

    private String investmentTitle;

    private Date investmentTime;

    private String invsetmentContent;

    private Integer investmentPrice;

    private static final long serialVersionUID = 1L;

    public Integer getInvestmentId() {
        return investmentId;
    }

    public void setInvestmentId(Integer investmentId) {
        this.investmentId = investmentId;
    }

    public Integer getHomeId() {
        return homeId;
    }

    public void setHomeId(Integer homeId) {
        this.homeId = homeId;
    }

    public String getInvestmentTitle() {
        return investmentTitle;
    }

    public void setInvestmentTitle(String investmentTitle) {
        this.investmentTitle = investmentTitle;
    }

    public Date getInvestmentTime() {
        return investmentTime;
    }

    public void setInvestmentTime(Date investmentTime) {
        this.investmentTime = investmentTime;
    }

    public String getInvsetmentContent() {
        return invsetmentContent;
    }

    public void setInvsetmentContent(String invsetmentContent) {
        this.invsetmentContent = invsetmentContent;
    }

    public Integer getInvestmentPrice() {
        return investmentPrice;
    }

    public void setInvestmentPrice(Integer investmentPrice) {
        this.investmentPrice = investmentPrice;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Investment other = (Investment) that;
        return (this.getInvestmentId() == null ? other.getInvestmentId() == null : this.getInvestmentId().equals(other.getInvestmentId()))
            && (this.getHomeId() == null ? other.getHomeId() == null : this.getHomeId().equals(other.getHomeId()))
            && (this.getInvestmentTitle() == null ? other.getInvestmentTitle() == null : this.getInvestmentTitle().equals(other.getInvestmentTitle()))
            && (this.getInvestmentTime() == null ? other.getInvestmentTime() == null : this.getInvestmentTime().equals(other.getInvestmentTime()))
            && (this.getInvsetmentContent() == null ? other.getInvsetmentContent() == null : this.getInvsetmentContent().equals(other.getInvsetmentContent()))
            && (this.getInvestmentPrice() == null ? other.getInvestmentPrice() == null : this.getInvestmentPrice().equals(other.getInvestmentPrice()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getInvestmentId() == null) ? 0 : getInvestmentId().hashCode());
        result = prime * result + ((getHomeId() == null) ? 0 : getHomeId().hashCode());
        result = prime * result + ((getInvestmentTitle() == null) ? 0 : getInvestmentTitle().hashCode());
        result = prime * result + ((getInvestmentTime() == null) ? 0 : getInvestmentTime().hashCode());
        result = prime * result + ((getInvsetmentContent() == null) ? 0 : getInvsetmentContent().hashCode());
        result = prime * result + ((getInvestmentPrice() == null) ? 0 : getInvestmentPrice().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", investmentId=").append(investmentId);
        sb.append(", homeId=").append(homeId);
        sb.append(", investmentTitle=").append(investmentTitle);
        sb.append(", investmentTime=").append(investmentTime);
        sb.append(", invsetmentContent=").append(invsetmentContent);
        sb.append(", investmentPrice=").append(investmentPrice);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}