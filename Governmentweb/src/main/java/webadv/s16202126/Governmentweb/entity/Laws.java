package webadv.s16202126.Governmentweb.entity;

import java.io.Serializable;

/**
 * laws
 * @author 
 */
public class Laws implements Serializable {
    private Integer lawId;

    private Integer investmentId;

    private Integer policiesId;

    private String lawTitle;

    private String lawContent;

    private static final long serialVersionUID = 1L;

    public Integer getLawId() {
        return lawId;
    }

    public void setLawId(Integer lawId) {
        this.lawId = lawId;
    }

    public Integer getInvestmentId() {
        return investmentId;
    }

    public void setInvestmentId(Integer investmentId) {
        this.investmentId = investmentId;
    }

    public Integer getPoliciesId() {
        return policiesId;
    }

    public void setPoliciesId(Integer policiesId) {
        this.policiesId = policiesId;
    }

    public String getLawTitle() {
        return lawTitle;
    }

    public void setLawTitle(String lawTitle) {
        this.lawTitle = lawTitle;
    }

    public String getLawContent() {
        return lawContent;
    }

    public void setLawContent(String lawContent) {
        this.lawContent = lawContent;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Laws other = (Laws) that;
        return (this.getLawId() == null ? other.getLawId() == null : this.getLawId().equals(other.getLawId()))
            && (this.getInvestmentId() == null ? other.getInvestmentId() == null : this.getInvestmentId().equals(other.getInvestmentId()))
            && (this.getPoliciesId() == null ? other.getPoliciesId() == null : this.getPoliciesId().equals(other.getPoliciesId()))
            && (this.getLawTitle() == null ? other.getLawTitle() == null : this.getLawTitle().equals(other.getLawTitle()))
            && (this.getLawContent() == null ? other.getLawContent() == null : this.getLawContent().equals(other.getLawContent()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getLawId() == null) ? 0 : getLawId().hashCode());
        result = prime * result + ((getInvestmentId() == null) ? 0 : getInvestmentId().hashCode());
        result = prime * result + ((getPoliciesId() == null) ? 0 : getPoliciesId().hashCode());
        result = prime * result + ((getLawTitle() == null) ? 0 : getLawTitle().hashCode());
        result = prime * result + ((getLawContent() == null) ? 0 : getLawContent().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", lawId=").append(lawId);
        sb.append(", investmentId=").append(investmentId);
        sb.append(", policiesId=").append(policiesId);
        sb.append(", lawTitle=").append(lawTitle);
        sb.append(", lawContent=").append(lawContent);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}