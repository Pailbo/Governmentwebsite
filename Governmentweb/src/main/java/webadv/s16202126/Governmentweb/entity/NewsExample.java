package webadv.s16202126.Governmentweb.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public NewsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andNewIdIsNull() {
            addCriterion("new_id is null");
            return (Criteria) this;
        }

        public Criteria andNewIdIsNotNull() {
            addCriterion("new_id is not null");
            return (Criteria) this;
        }

        public Criteria andNewIdEqualTo(Integer value) {
            addCriterion("new_id =", value, "newId");
            return (Criteria) this;
        }

        public Criteria andNewIdNotEqualTo(Integer value) {
            addCriterion("new_id <>", value, "newId");
            return (Criteria) this;
        }

        public Criteria andNewIdGreaterThan(Integer value) {
            addCriterion("new_id >", value, "newId");
            return (Criteria) this;
        }

        public Criteria andNewIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("new_id >=", value, "newId");
            return (Criteria) this;
        }

        public Criteria andNewIdLessThan(Integer value) {
            addCriterion("new_id <", value, "newId");
            return (Criteria) this;
        }

        public Criteria andNewIdLessThanOrEqualTo(Integer value) {
            addCriterion("new_id <=", value, "newId");
            return (Criteria) this;
        }

        public Criteria andNewIdIn(List<Integer> values) {
            addCriterion("new_id in", values, "newId");
            return (Criteria) this;
        }

        public Criteria andNewIdNotIn(List<Integer> values) {
            addCriterion("new_id not in", values, "newId");
            return (Criteria) this;
        }

        public Criteria andNewIdBetween(Integer value1, Integer value2) {
            addCriterion("new_id between", value1, value2, "newId");
            return (Criteria) this;
        }

        public Criteria andNewIdNotBetween(Integer value1, Integer value2) {
            addCriterion("new_id not between", value1, value2, "newId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNull() {
            addCriterion("home_id is null");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNotNull() {
            addCriterion("home_id is not null");
            return (Criteria) this;
        }

        public Criteria andHomeIdEqualTo(Integer value) {
            addCriterion("home_id =", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotEqualTo(Integer value) {
            addCriterion("home_id <>", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThan(Integer value) {
            addCriterion("home_id >", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("home_id >=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThan(Integer value) {
            addCriterion("home_id <", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThanOrEqualTo(Integer value) {
            addCriterion("home_id <=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIn(List<Integer> values) {
            addCriterion("home_id in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotIn(List<Integer> values) {
            addCriterion("home_id not in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdBetween(Integer value1, Integer value2) {
            addCriterion("home_id between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("home_id not between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andNewImageIsNull() {
            addCriterion("new_image is null");
            return (Criteria) this;
        }

        public Criteria andNewImageIsNotNull() {
            addCriterion("new_image is not null");
            return (Criteria) this;
        }

        public Criteria andNewImageEqualTo(String value) {
            addCriterion("new_image =", value, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageNotEqualTo(String value) {
            addCriterion("new_image <>", value, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageGreaterThan(String value) {
            addCriterion("new_image >", value, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageGreaterThanOrEqualTo(String value) {
            addCriterion("new_image >=", value, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageLessThan(String value) {
            addCriterion("new_image <", value, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageLessThanOrEqualTo(String value) {
            addCriterion("new_image <=", value, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageLike(String value) {
            addCriterion("new_image like", value, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageNotLike(String value) {
            addCriterion("new_image not like", value, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageIn(List<String> values) {
            addCriterion("new_image in", values, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageNotIn(List<String> values) {
            addCriterion("new_image not in", values, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageBetween(String value1, String value2) {
            addCriterion("new_image between", value1, value2, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewImageNotBetween(String value1, String value2) {
            addCriterion("new_image not between", value1, value2, "newImage");
            return (Criteria) this;
        }

        public Criteria andNewLinkIsNull() {
            addCriterion("new_link is null");
            return (Criteria) this;
        }

        public Criteria andNewLinkIsNotNull() {
            addCriterion("new_link is not null");
            return (Criteria) this;
        }

        public Criteria andNewLinkEqualTo(String value) {
            addCriterion("new_link =", value, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkNotEqualTo(String value) {
            addCriterion("new_link <>", value, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkGreaterThan(String value) {
            addCriterion("new_link >", value, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkGreaterThanOrEqualTo(String value) {
            addCriterion("new_link >=", value, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkLessThan(String value) {
            addCriterion("new_link <", value, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkLessThanOrEqualTo(String value) {
            addCriterion("new_link <=", value, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkLike(String value) {
            addCriterion("new_link like", value, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkNotLike(String value) {
            addCriterion("new_link not like", value, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkIn(List<String> values) {
            addCriterion("new_link in", values, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkNotIn(List<String> values) {
            addCriterion("new_link not in", values, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkBetween(String value1, String value2) {
            addCriterion("new_link between", value1, value2, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewLinkNotBetween(String value1, String value2) {
            addCriterion("new_link not between", value1, value2, "newLink");
            return (Criteria) this;
        }

        public Criteria andNewTiemIsNull() {
            addCriterion("new_tiem is null");
            return (Criteria) this;
        }

        public Criteria andNewTiemIsNotNull() {
            addCriterion("new_tiem is not null");
            return (Criteria) this;
        }

        public Criteria andNewTiemEqualTo(Date value) {
            addCriterion("new_tiem =", value, "newTiem");
            return (Criteria) this;
        }

        public Criteria andNewTiemNotEqualTo(Date value) {
            addCriterion("new_tiem <>", value, "newTiem");
            return (Criteria) this;
        }

        public Criteria andNewTiemGreaterThan(Date value) {
            addCriterion("new_tiem >", value, "newTiem");
            return (Criteria) this;
        }

        public Criteria andNewTiemGreaterThanOrEqualTo(Date value) {
            addCriterion("new_tiem >=", value, "newTiem");
            return (Criteria) this;
        }

        public Criteria andNewTiemLessThan(Date value) {
            addCriterion("new_tiem <", value, "newTiem");
            return (Criteria) this;
        }

        public Criteria andNewTiemLessThanOrEqualTo(Date value) {
            addCriterion("new_tiem <=", value, "newTiem");
            return (Criteria) this;
        }

        public Criteria andNewTiemIn(List<Date> values) {
            addCriterion("new_tiem in", values, "newTiem");
            return (Criteria) this;
        }

        public Criteria andNewTiemNotIn(List<Date> values) {
            addCriterion("new_tiem not in", values, "newTiem");
            return (Criteria) this;
        }

        public Criteria andNewTiemBetween(Date value1, Date value2) {
            addCriterion("new_tiem between", value1, value2, "newTiem");
            return (Criteria) this;
        }

        public Criteria andNewTiemNotBetween(Date value1, Date value2) {
            addCriterion("new_tiem not between", value1, value2, "newTiem");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}