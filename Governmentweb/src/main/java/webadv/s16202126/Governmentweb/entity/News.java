package webadv.s16202126.Governmentweb.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * news
 * @author 
 */
public class News implements Serializable {
    private Integer newId;

    private Integer homeId;

    private String newImage;

    private String newLink;

    private Date newTiem;

    private static final long serialVersionUID = 1L;

    public Integer getNewId() {
        return newId;
    }

    public void setNewId(Integer newId) {
        this.newId = newId;
    }

    public Integer getHomeId() {
        return homeId;
    }

    public void setHomeId(Integer homeId) {
        this.homeId = homeId;
    }

    public String getNewImage() {
        return newImage;
    }

    public void setNewImage(String newImage) {
        this.newImage = newImage;
    }

    public String getNewLink() {
        return newLink;
    }

    public void setNewLink(String newLink) {
        this.newLink = newLink;
    }

    public Date getNewTiem() {
        return newTiem;
    }

    public void setNewTiem(Date newTiem) {
        this.newTiem = newTiem;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        News other = (News) that;
        return (this.getNewId() == null ? other.getNewId() == null : this.getNewId().equals(other.getNewId()))
            && (this.getHomeId() == null ? other.getHomeId() == null : this.getHomeId().equals(other.getHomeId()))
            && (this.getNewImage() == null ? other.getNewImage() == null : this.getNewImage().equals(other.getNewImage()))
            && (this.getNewLink() == null ? other.getNewLink() == null : this.getNewLink().equals(other.getNewLink()))
            && (this.getNewTiem() == null ? other.getNewTiem() == null : this.getNewTiem().equals(other.getNewTiem()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getNewId() == null) ? 0 : getNewId().hashCode());
        result = prime * result + ((getHomeId() == null) ? 0 : getHomeId().hashCode());
        result = prime * result + ((getNewImage() == null) ? 0 : getNewImage().hashCode());
        result = prime * result + ((getNewLink() == null) ? 0 : getNewLink().hashCode());
        result = prime * result + ((getNewTiem() == null) ? 0 : getNewTiem().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", newId=").append(newId);
        sb.append(", homeId=").append(homeId);
        sb.append(", newImage=").append(newImage);
        sb.append(", newLink=").append(newLink);
        sb.append(", newTiem=").append(newTiem);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}