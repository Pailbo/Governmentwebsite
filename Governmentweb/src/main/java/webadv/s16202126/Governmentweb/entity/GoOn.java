package webadv.s16202126.Governmentweb.entity;

import java.io.Serializable;

/**
 * go_on
 * @author 
 */
public class GoOn implements Serializable {
    private Integer goId;

    private Long homeId;

    private String goNane;

    private String goLink;

    private static final long serialVersionUID = 1L;

    public Integer getGoId() {
        return goId;
    }

    public void setGoId(Integer goId) {
        this.goId = goId;
    }

    public Long getHomeId() {
        return homeId;
    }

    public void setHomeId(Long homeId) {
        this.homeId = homeId;
    }

    public String getGoNane() {
        return goNane;
    }

    public void setGoNane(String goNane) {
        this.goNane = goNane;
    }

    public String getGoLink() {
        return goLink;
    }

    public void setGoLink(String goLink) {
        this.goLink = goLink;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        GoOn other = (GoOn) that;
        return (this.getGoId() == null ? other.getGoId() == null : this.getGoId().equals(other.getGoId()))
            && (this.getHomeId() == null ? other.getHomeId() == null : this.getHomeId().equals(other.getHomeId()))
            && (this.getGoNane() == null ? other.getGoNane() == null : this.getGoNane().equals(other.getGoNane()))
            && (this.getGoLink() == null ? other.getGoLink() == null : this.getGoLink().equals(other.getGoLink()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getGoId() == null) ? 0 : getGoId().hashCode());
        result = prime * result + ((getHomeId() == null) ? 0 : getHomeId().hashCode());
        result = prime * result + ((getGoNane() == null) ? 0 : getGoNane().hashCode());
        result = prime * result + ((getGoLink() == null) ? 0 : getGoLink().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", goId=").append(goId);
        sb.append(", homeId=").append(homeId);
        sb.append(", goNane=").append(goNane);
        sb.append(", goLink=").append(goLink);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}