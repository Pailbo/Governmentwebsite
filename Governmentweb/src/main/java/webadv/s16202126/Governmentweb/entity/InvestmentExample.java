package webadv.s16202126.Governmentweb.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InvestmentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public InvestmentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andInvestmentIdIsNull() {
            addCriterion("investment_id is null");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdIsNotNull() {
            addCriterion("investment_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdEqualTo(Integer value) {
            addCriterion("investment_id =", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdNotEqualTo(Integer value) {
            addCriterion("investment_id <>", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdGreaterThan(Integer value) {
            addCriterion("investment_id >", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("investment_id >=", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdLessThan(Integer value) {
            addCriterion("investment_id <", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdLessThanOrEqualTo(Integer value) {
            addCriterion("investment_id <=", value, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdIn(List<Integer> values) {
            addCriterion("investment_id in", values, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdNotIn(List<Integer> values) {
            addCriterion("investment_id not in", values, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdBetween(Integer value1, Integer value2) {
            addCriterion("investment_id between", value1, value2, "investmentId");
            return (Criteria) this;
        }

        public Criteria andInvestmentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("investment_id not between", value1, value2, "investmentId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNull() {
            addCriterion("home_id is null");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNotNull() {
            addCriterion("home_id is not null");
            return (Criteria) this;
        }

        public Criteria andHomeIdEqualTo(Integer value) {
            addCriterion("home_id =", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotEqualTo(Integer value) {
            addCriterion("home_id <>", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThan(Integer value) {
            addCriterion("home_id >", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("home_id >=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThan(Integer value) {
            addCriterion("home_id <", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThanOrEqualTo(Integer value) {
            addCriterion("home_id <=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIn(List<Integer> values) {
            addCriterion("home_id in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotIn(List<Integer> values) {
            addCriterion("home_id not in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdBetween(Integer value1, Integer value2) {
            addCriterion("home_id between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("home_id not between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleIsNull() {
            addCriterion("investment_title is null");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleIsNotNull() {
            addCriterion("investment_title is not null");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleEqualTo(String value) {
            addCriterion("investment_title =", value, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleNotEqualTo(String value) {
            addCriterion("investment_title <>", value, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleGreaterThan(String value) {
            addCriterion("investment_title >", value, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleGreaterThanOrEqualTo(String value) {
            addCriterion("investment_title >=", value, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleLessThan(String value) {
            addCriterion("investment_title <", value, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleLessThanOrEqualTo(String value) {
            addCriterion("investment_title <=", value, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleLike(String value) {
            addCriterion("investment_title like", value, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleNotLike(String value) {
            addCriterion("investment_title not like", value, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleIn(List<String> values) {
            addCriterion("investment_title in", values, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleNotIn(List<String> values) {
            addCriterion("investment_title not in", values, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleBetween(String value1, String value2) {
            addCriterion("investment_title between", value1, value2, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTitleNotBetween(String value1, String value2) {
            addCriterion("investment_title not between", value1, value2, "investmentTitle");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeIsNull() {
            addCriterion("investment_time is null");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeIsNotNull() {
            addCriterion("investment_time is not null");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeEqualTo(Date value) {
            addCriterion("investment_time =", value, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeNotEqualTo(Date value) {
            addCriterion("investment_time <>", value, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeGreaterThan(Date value) {
            addCriterion("investment_time >", value, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("investment_time >=", value, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeLessThan(Date value) {
            addCriterion("investment_time <", value, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeLessThanOrEqualTo(Date value) {
            addCriterion("investment_time <=", value, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeIn(List<Date> values) {
            addCriterion("investment_time in", values, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeNotIn(List<Date> values) {
            addCriterion("investment_time not in", values, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeBetween(Date value1, Date value2) {
            addCriterion("investment_time between", value1, value2, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvestmentTimeNotBetween(Date value1, Date value2) {
            addCriterion("investment_time not between", value1, value2, "investmentTime");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentIsNull() {
            addCriterion("invsetment_content is null");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentIsNotNull() {
            addCriterion("invsetment_content is not null");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentEqualTo(String value) {
            addCriterion("invsetment_content =", value, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentNotEqualTo(String value) {
            addCriterion("invsetment_content <>", value, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentGreaterThan(String value) {
            addCriterion("invsetment_content >", value, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentGreaterThanOrEqualTo(String value) {
            addCriterion("invsetment_content >=", value, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentLessThan(String value) {
            addCriterion("invsetment_content <", value, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentLessThanOrEqualTo(String value) {
            addCriterion("invsetment_content <=", value, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentLike(String value) {
            addCriterion("invsetment_content like", value, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentNotLike(String value) {
            addCriterion("invsetment_content not like", value, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentIn(List<String> values) {
            addCriterion("invsetment_content in", values, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentNotIn(List<String> values) {
            addCriterion("invsetment_content not in", values, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentBetween(String value1, String value2) {
            addCriterion("invsetment_content between", value1, value2, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvsetmentContentNotBetween(String value1, String value2) {
            addCriterion("invsetment_content not between", value1, value2, "invsetmentContent");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceIsNull() {
            addCriterion("investment_price is null");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceIsNotNull() {
            addCriterion("investment_price is not null");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceEqualTo(Integer value) {
            addCriterion("investment_price =", value, "investmentPrice");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceNotEqualTo(Integer value) {
            addCriterion("investment_price <>", value, "investmentPrice");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceGreaterThan(Integer value) {
            addCriterion("investment_price >", value, "investmentPrice");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("investment_price >=", value, "investmentPrice");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceLessThan(Integer value) {
            addCriterion("investment_price <", value, "investmentPrice");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceLessThanOrEqualTo(Integer value) {
            addCriterion("investment_price <=", value, "investmentPrice");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceIn(List<Integer> values) {
            addCriterion("investment_price in", values, "investmentPrice");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceNotIn(List<Integer> values) {
            addCriterion("investment_price not in", values, "investmentPrice");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceBetween(Integer value1, Integer value2) {
            addCriterion("investment_price between", value1, value2, "investmentPrice");
            return (Criteria) this;
        }

        public Criteria andInvestmentPriceNotBetween(Integer value1, Integer value2) {
            addCriterion("investment_price not between", value1, value2, "investmentPrice");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}