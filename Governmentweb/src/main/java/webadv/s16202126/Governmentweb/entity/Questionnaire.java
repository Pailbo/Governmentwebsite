package webadv.s16202126.Governmentweb.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * questionnaire
 * @author 
 */
public class Questionnaire implements Serializable {
    private Integer questionnaireId;

    private Integer homeId;

    private Date questionnaireStart;

    private Date questionnaireEnd;

    private String questionnaireContent;

    private static final long serialVersionUID = 1L;

    public Integer getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(Integer questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public Integer getHomeId() {
        return homeId;
    }

    public void setHomeId(Integer homeId) {
        this.homeId = homeId;
    }

    public Date getQuestionnaireStart() {
        return questionnaireStart;
    }

    public void setQuestionnaireStart(Date questionnaireStart) {
        this.questionnaireStart = questionnaireStart;
    }

    public Date getQuestionnaireEnd() {
        return questionnaireEnd;
    }

    public void setQuestionnaireEnd(Date questionnaireEnd) {
        this.questionnaireEnd = questionnaireEnd;
    }

    public String getQuestionnaireContent() {
        return questionnaireContent;
    }

    public void setQuestionnaireContent(String questionnaireContent) {
        this.questionnaireContent = questionnaireContent;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Questionnaire other = (Questionnaire) that;
        return (this.getQuestionnaireId() == null ? other.getQuestionnaireId() == null : this.getQuestionnaireId().equals(other.getQuestionnaireId()))
            && (this.getHomeId() == null ? other.getHomeId() == null : this.getHomeId().equals(other.getHomeId()))
            && (this.getQuestionnaireStart() == null ? other.getQuestionnaireStart() == null : this.getQuestionnaireStart().equals(other.getQuestionnaireStart()))
            && (this.getQuestionnaireEnd() == null ? other.getQuestionnaireEnd() == null : this.getQuestionnaireEnd().equals(other.getQuestionnaireEnd()))
            && (this.getQuestionnaireContent() == null ? other.getQuestionnaireContent() == null : this.getQuestionnaireContent().equals(other.getQuestionnaireContent()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getQuestionnaireId() == null) ? 0 : getQuestionnaireId().hashCode());
        result = prime * result + ((getHomeId() == null) ? 0 : getHomeId().hashCode());
        result = prime * result + ((getQuestionnaireStart() == null) ? 0 : getQuestionnaireStart().hashCode());
        result = prime * result + ((getQuestionnaireEnd() == null) ? 0 : getQuestionnaireEnd().hashCode());
        result = prime * result + ((getQuestionnaireContent() == null) ? 0 : getQuestionnaireContent().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", questionnaireId=").append(questionnaireId);
        sb.append(", homeId=").append(homeId);
        sb.append(", questionnaireStart=").append(questionnaireStart);
        sb.append(", questionnaireEnd=").append(questionnaireEnd);
        sb.append(", questionnaireContent=").append(questionnaireContent);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}