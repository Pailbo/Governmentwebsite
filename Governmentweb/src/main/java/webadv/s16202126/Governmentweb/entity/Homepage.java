package webadv.s16202126.Governmentweb.entity;

import java.io.Serializable;

/**
 * homepage
 * @author 
 */
public class Homepage implements Serializable {
    private Integer homeId;

    private static final long serialVersionUID = 1L;

    public Integer getHomeId() {
        return homeId;
    }

    public void setHomeId(Integer homeId) {
        this.homeId = homeId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Homepage other = (Homepage) that;
        return (this.getHomeId() == null ? other.getHomeId() == null : this.getHomeId().equals(other.getHomeId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getHomeId() == null) ? 0 : getHomeId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", homeId=").append(homeId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}