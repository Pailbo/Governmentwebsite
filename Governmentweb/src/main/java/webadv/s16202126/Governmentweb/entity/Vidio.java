package webadv.s16202126.Governmentweb.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * vidio
 * @author 
 */
public class Vidio implements Serializable {
    private Integer vidioId;

    private Integer homeId;

    private String vidioName;

    private String vidioLink;

    private Date vidioTime;

    private static final long serialVersionUID = 1L;

    public Integer getVidioId() {
        return vidioId;
    }

    public void setVidioId(Integer vidioId) {
        this.vidioId = vidioId;
    }

    public Integer getHomeId() {
        return homeId;
    }

    public void setHomeId(Integer homeId) {
        this.homeId = homeId;
    }

    public String getVidioName() {
        return vidioName;
    }

    public void setVidioName(String vidioName) {
        this.vidioName = vidioName;
    }

    public String getVidioLink() {
        return vidioLink;
    }

    public void setVidioLink(String vidioLink) {
        this.vidioLink = vidioLink;
    }

    public Date getVidioTime() {
        return vidioTime;
    }

    public void setVidioTime(Date vidioTime) {
        this.vidioTime = vidioTime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Vidio other = (Vidio) that;
        return (this.getVidioId() == null ? other.getVidioId() == null : this.getVidioId().equals(other.getVidioId()))
            && (this.getHomeId() == null ? other.getHomeId() == null : this.getHomeId().equals(other.getHomeId()))
            && (this.getVidioName() == null ? other.getVidioName() == null : this.getVidioName().equals(other.getVidioName()))
            && (this.getVidioLink() == null ? other.getVidioLink() == null : this.getVidioLink().equals(other.getVidioLink()))
            && (this.getVidioTime() == null ? other.getVidioTime() == null : this.getVidioTime().equals(other.getVidioTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getVidioId() == null) ? 0 : getVidioId().hashCode());
        result = prime * result + ((getHomeId() == null) ? 0 : getHomeId().hashCode());
        result = prime * result + ((getVidioName() == null) ? 0 : getVidioName().hashCode());
        result = prime * result + ((getVidioLink() == null) ? 0 : getVidioLink().hashCode());
        result = prime * result + ((getVidioTime() == null) ? 0 : getVidioTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", vidioId=").append(vidioId);
        sb.append(", homeId=").append(homeId);
        sb.append(", vidioName=").append(vidioName);
        sb.append(", vidioLink=").append(vidioLink);
        sb.append(", vidioTime=").append(vidioTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}