package webadv.s16202126.Governmentweb.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * schedule
 * @author 
 */
public class Schedule implements Serializable {
    private Integer scheduleId;

    private Integer homeId;

    private Date scheduleTiem;

    private String schedulePerson;

    private String scheduleEven;

    private static final long serialVersionUID = 1L;

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Integer getHomeId() {
        return homeId;
    }

    public void setHomeId(Integer homeId) {
        this.homeId = homeId;
    }

    public Date getScheduleTiem() {
        return scheduleTiem;
    }

    public void setScheduleTiem(Date scheduleTiem) {
        this.scheduleTiem = scheduleTiem;
    }

    public String getSchedulePerson() {
        return schedulePerson;
    }

    public void setSchedulePerson(String schedulePerson) {
        this.schedulePerson = schedulePerson;
    }

    public String getScheduleEven() {
        return scheduleEven;
    }

    public void setScheduleEven(String scheduleEven) {
        this.scheduleEven = scheduleEven;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Schedule other = (Schedule) that;
        return (this.getScheduleId() == null ? other.getScheduleId() == null : this.getScheduleId().equals(other.getScheduleId()))
            && (this.getHomeId() == null ? other.getHomeId() == null : this.getHomeId().equals(other.getHomeId()))
            && (this.getScheduleTiem() == null ? other.getScheduleTiem() == null : this.getScheduleTiem().equals(other.getScheduleTiem()))
            && (this.getSchedulePerson() == null ? other.getSchedulePerson() == null : this.getSchedulePerson().equals(other.getSchedulePerson()))
            && (this.getScheduleEven() == null ? other.getScheduleEven() == null : this.getScheduleEven().equals(other.getScheduleEven()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getScheduleId() == null) ? 0 : getScheduleId().hashCode());
        result = prime * result + ((getHomeId() == null) ? 0 : getHomeId().hashCode());
        result = prime * result + ((getScheduleTiem() == null) ? 0 : getScheduleTiem().hashCode());
        result = prime * result + ((getSchedulePerson() == null) ? 0 : getSchedulePerson().hashCode());
        result = prime * result + ((getScheduleEven() == null) ? 0 : getScheduleEven().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", scheduleId=").append(scheduleId);
        sb.append(", homeId=").append(homeId);
        sb.append(", scheduleTiem=").append(scheduleTiem);
        sb.append(", schedulePerson=").append(schedulePerson);
        sb.append(", scheduleEven=").append(scheduleEven);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}