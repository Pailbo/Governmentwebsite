package webadv.s16202126.Governmentweb.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QuestionnaireExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public QuestionnaireExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andQuestionnaireIdIsNull() {
            addCriterion("Questionnaire_id is null");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdIsNotNull() {
            addCriterion("Questionnaire_id is not null");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdEqualTo(Integer value) {
            addCriterion("Questionnaire_id =", value, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdNotEqualTo(Integer value) {
            addCriterion("Questionnaire_id <>", value, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdGreaterThan(Integer value) {
            addCriterion("Questionnaire_id >", value, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Questionnaire_id >=", value, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdLessThan(Integer value) {
            addCriterion("Questionnaire_id <", value, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdLessThanOrEqualTo(Integer value) {
            addCriterion("Questionnaire_id <=", value, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdIn(List<Integer> values) {
            addCriterion("Questionnaire_id in", values, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdNotIn(List<Integer> values) {
            addCriterion("Questionnaire_id not in", values, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdBetween(Integer value1, Integer value2) {
            addCriterion("Questionnaire_id between", value1, value2, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Questionnaire_id not between", value1, value2, "questionnaireId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNull() {
            addCriterion("home_id is null");
            return (Criteria) this;
        }

        public Criteria andHomeIdIsNotNull() {
            addCriterion("home_id is not null");
            return (Criteria) this;
        }

        public Criteria andHomeIdEqualTo(Integer value) {
            addCriterion("home_id =", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotEqualTo(Integer value) {
            addCriterion("home_id <>", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThan(Integer value) {
            addCriterion("home_id >", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("home_id >=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThan(Integer value) {
            addCriterion("home_id <", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdLessThanOrEqualTo(Integer value) {
            addCriterion("home_id <=", value, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdIn(List<Integer> values) {
            addCriterion("home_id in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotIn(List<Integer> values) {
            addCriterion("home_id not in", values, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdBetween(Integer value1, Integer value2) {
            addCriterion("home_id between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andHomeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("home_id not between", value1, value2, "homeId");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartIsNull() {
            addCriterion("Questionnaire_start is null");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartIsNotNull() {
            addCriterion("Questionnaire_start is not null");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartEqualTo(Date value) {
            addCriterion("Questionnaire_start =", value, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartNotEqualTo(Date value) {
            addCriterion("Questionnaire_start <>", value, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartGreaterThan(Date value) {
            addCriterion("Questionnaire_start >", value, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartGreaterThanOrEqualTo(Date value) {
            addCriterion("Questionnaire_start >=", value, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartLessThan(Date value) {
            addCriterion("Questionnaire_start <", value, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartLessThanOrEqualTo(Date value) {
            addCriterion("Questionnaire_start <=", value, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartIn(List<Date> values) {
            addCriterion("Questionnaire_start in", values, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartNotIn(List<Date> values) {
            addCriterion("Questionnaire_start not in", values, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartBetween(Date value1, Date value2) {
            addCriterion("Questionnaire_start between", value1, value2, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireStartNotBetween(Date value1, Date value2) {
            addCriterion("Questionnaire_start not between", value1, value2, "questionnaireStart");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndIsNull() {
            addCriterion("Questionnaire_end is null");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndIsNotNull() {
            addCriterion("Questionnaire_end is not null");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndEqualTo(Date value) {
            addCriterion("Questionnaire_end =", value, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndNotEqualTo(Date value) {
            addCriterion("Questionnaire_end <>", value, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndGreaterThan(Date value) {
            addCriterion("Questionnaire_end >", value, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndGreaterThanOrEqualTo(Date value) {
            addCriterion("Questionnaire_end >=", value, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndLessThan(Date value) {
            addCriterion("Questionnaire_end <", value, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndLessThanOrEqualTo(Date value) {
            addCriterion("Questionnaire_end <=", value, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndIn(List<Date> values) {
            addCriterion("Questionnaire_end in", values, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndNotIn(List<Date> values) {
            addCriterion("Questionnaire_end not in", values, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndBetween(Date value1, Date value2) {
            addCriterion("Questionnaire_end between", value1, value2, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireEndNotBetween(Date value1, Date value2) {
            addCriterion("Questionnaire_end not between", value1, value2, "questionnaireEnd");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentIsNull() {
            addCriterion("Questionnaire_content is null");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentIsNotNull() {
            addCriterion("Questionnaire_content is not null");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentEqualTo(String value) {
            addCriterion("Questionnaire_content =", value, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentNotEqualTo(String value) {
            addCriterion("Questionnaire_content <>", value, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentGreaterThan(String value) {
            addCriterion("Questionnaire_content >", value, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentGreaterThanOrEqualTo(String value) {
            addCriterion("Questionnaire_content >=", value, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentLessThan(String value) {
            addCriterion("Questionnaire_content <", value, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentLessThanOrEqualTo(String value) {
            addCriterion("Questionnaire_content <=", value, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentLike(String value) {
            addCriterion("Questionnaire_content like", value, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentNotLike(String value) {
            addCriterion("Questionnaire_content not like", value, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentIn(List<String> values) {
            addCriterion("Questionnaire_content in", values, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentNotIn(List<String> values) {
            addCriterion("Questionnaire_content not in", values, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentBetween(String value1, String value2) {
            addCriterion("Questionnaire_content between", value1, value2, "questionnaireContent");
            return (Criteria) this;
        }

        public Criteria andQuestionnaireContentNotBetween(String value1, String value2) {
            addCriterion("Questionnaire_content not between", value1, value2, "questionnaireContent");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}