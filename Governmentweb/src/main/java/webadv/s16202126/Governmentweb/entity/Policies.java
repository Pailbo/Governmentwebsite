package webadv.s16202126.Governmentweb.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * policies
 * @author 
 */
public class Policies implements Serializable {
    private Integer policiesId;

    private String policiesTitle;

    private Date policiesTime;

    private static final long serialVersionUID = 1L;

    public Integer getPoliciesId() {
        return policiesId;
    }

    public void setPoliciesId(Integer policiesId) {
        this.policiesId = policiesId;
    }

    public String getPoliciesTitle() {
        return policiesTitle;
    }

    public void setPoliciesTitle(String policiesTitle) {
        this.policiesTitle = policiesTitle;
    }

    public Date getPoliciesTime() {
        return policiesTime;
    }

    public void setPoliciesTime(Date policiesTime) {
        this.policiesTime = policiesTime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Policies other = (Policies) that;
        return (this.getPoliciesId() == null ? other.getPoliciesId() == null : this.getPoliciesId().equals(other.getPoliciesId()))
            && (this.getPoliciesTitle() == null ? other.getPoliciesTitle() == null : this.getPoliciesTitle().equals(other.getPoliciesTitle()))
            && (this.getPoliciesTime() == null ? other.getPoliciesTime() == null : this.getPoliciesTime().equals(other.getPoliciesTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPoliciesId() == null) ? 0 : getPoliciesId().hashCode());
        result = prime * result + ((getPoliciesTitle() == null) ? 0 : getPoliciesTitle().hashCode());
        result = prime * result + ((getPoliciesTime() == null) ? 0 : getPoliciesTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", policiesId=").append(policiesId);
        sb.append(", policiesTitle=").append(policiesTitle);
        sb.append(", policiesTime=").append(policiesTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}