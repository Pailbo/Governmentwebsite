package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.Policies;
import webadv.s16202126.Governmentweb.entity.PoliciesExample;

/**
 * Policiesrepository继承基类
 */
@Repository
public interface Policiesrepository extends MyBatisBaseDao<Policies, Integer, PoliciesExample> {
}