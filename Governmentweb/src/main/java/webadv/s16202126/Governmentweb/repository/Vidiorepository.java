package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.Vidio;
import webadv.s16202126.Governmentweb.entity.VidioExample;

/**
 * Vidiorepository继承基类
 */
@Repository
public interface Vidiorepository extends MyBatisBaseDao<Vidio, Integer, VidioExample> {
}