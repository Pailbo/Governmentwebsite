package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.Schedule;
import webadv.s16202126.Governmentweb.entity.ScheduleExample;

/**
 * Schedulerepository继承基类
 */
@Repository
public interface Schedulerepository extends MyBatisBaseDao<Schedule, Integer, ScheduleExample> {
}