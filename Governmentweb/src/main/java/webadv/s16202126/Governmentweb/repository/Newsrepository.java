package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.News;
import webadv.s16202126.Governmentweb.entity.NewsExample;

/**
 * Newsrepository继承基类
 */
@Repository
public interface Newsrepository extends MyBatisBaseDao<News, Integer, NewsExample> {
}