package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.Message;
import webadv.s16202126.Governmentweb.entity.MessageExample;

/**
 * Messagerepository继承基类
 */
@Repository
public interface Messagerepository extends MyBatisBaseDao<Message, Integer, MessageExample> {
}