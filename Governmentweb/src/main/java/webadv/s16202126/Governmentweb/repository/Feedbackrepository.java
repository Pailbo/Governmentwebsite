package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.Feedback;
import webadv.s16202126.Governmentweb.entity.FeedbackExample;

/**
 * Feedbackrepository继承基类
 */
@Repository
public interface Feedbackrepository extends MyBatisBaseDao<Feedback, Integer, FeedbackExample> {
}