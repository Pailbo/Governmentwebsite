package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.Homepage;
import webadv.s16202126.Governmentweb.entity.HomepageExample;

/**
 * Homepagerepository继承基类
 */
@Repository
public interface Homepagerepository extends MyBatisBaseDao<Homepage, Integer, HomepageExample> {
}