package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.Laws;
import webadv.s16202126.Governmentweb.entity.LawsExample;

/**
 * Lawsrepository继承基类
 */
@Repository
public interface Lawsrepository extends MyBatisBaseDao<Laws, Integer, LawsExample> {
}