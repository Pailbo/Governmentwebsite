package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.Investment;
import webadv.s16202126.Governmentweb.entity.InvestmentExample;

/**
 * Investmentrepository继承基类
 */
@Repository
public interface Investmentrepository extends MyBatisBaseDao<Investment, Integer, InvestmentExample> {
}