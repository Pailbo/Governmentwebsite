package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.User;
import webadv.s16202126.Governmentweb.entity.UserExample;

/**
 * Userrepository继承基类
 */
@Repository
public interface Userrepository extends MyBatisBaseDao<User, String, UserExample> {
}