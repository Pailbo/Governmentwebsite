package webadv.s16202126.Governmentweb.repository;

import org.springframework.stereotype.Repository;
import webadv.s16202126.Governmentweb.entity.GoOn;
import webadv.s16202126.Governmentweb.entity.GoOnExample;

/**
 * GoOnrepository继承基类
 */
@Repository
public interface GoOnrepository extends MyBatisBaseDao<GoOn, Integer, GoOnExample> {
}