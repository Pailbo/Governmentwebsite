package webadv.s16202126.Governmentweb.controller;



import java.util.Date;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;



import webadv.s16202126.Governmentweb.entity.News;
import webadv.s16202126.Governmentweb.entity.NewsExample;
import webadv.s16202126.Governmentweb.repository.Newsrepository;

@Controller
public class NewsController {
	@Autowired
	Newsrepository  newsMapper;

	@PostMapping("/news")
	public String NewsAdd(Model model,HttpSession session, String newsUrl, String title) {
		News news= new News();
		news.setNewLink(newsUrl);
		news.setNewImage(title);
		news.setNewTiem(new Date());
		newsMapper.insert(news);
		
			
		  return "redirect:/news";
		
	}
	
//	 NewsExample newsq= new   NewsExample();
//	 NewsExample.Criteria c=newsq.createCriteria();
	 
	@PostMapping("/Search")//搜索功能
	public String Search(Model model,HttpSession session, String search) {
		session.setAttribute("newsearch", 0);
		 NewsExample newsq= new   NewsExample();
		 search="%"+search+"%";//要加这句
		 System.out.println(search);
		 //组合查询
   	     NewsExample.Criteria c=newsq.createCriteria();
  	     c.andNewImageLike(search);
  	    
   	     List<News> searchNews=newsMapper.selectByExample(newsq);
   	     System.out.println(searchNews);
   	    System.out.println(searchNews.size());
   	    
   	     if (searchNews.size()==0) {
   	    	 
   	    	
			session.setAttribute("newsearch", 0);
   	    	 
			 return "redirect:/news";
		}
   	     
		else {
			   session.setAttribute("searchNews", searchNews);
				session.setAttribute("newsearch", 1);
				return "redirect:/news";
			}
		  
		
	}
	 
	@GetMapping("/news")
	public String news(Model model,HttpSession session) {
		List<News>  news =newsMapper.selectByExample(null);
		model.addAttribute("news", news);
		
		
		  return "news";
	}
	
	
	@PostMapping("/deletenew")
	public String deleteAdd(Model model,HttpSession session, String newsid ) {
	      	int id =Integer.valueOf(newsid);
    	  newsMapper.deleteByPrimaryKey(id);
		
			
    	  return "redirect:/news";

		
	}


}
