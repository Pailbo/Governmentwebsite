package webadv.s16202126.Governmentweb.controller;

import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import webadv.s16202126.Governmentweb.entity.Investment;


import webadv.s16202126.Governmentweb.repository.Investmentrepository;


import webadv.s16202126.Governmentweb.repository.Investmentrepository;


@Controller
public class InvestController {
	@Autowired
	Investmentrepository investmentMapper;

	// 添加
	@PostMapping("/investmentlist")
	public String addnewinvestmnet(Model model, HttpSession session, int investmentid, String investmenttitle,
			String investmenttime, String investmentconcent,String investmentprice) {

		Investment investment = new Investment();
		investment.setInvestmentId(investmentid);
		investment.setInvestmentTitle(investmenttitle);
		investment.setInvestmentPrice(Integer.parseInt(investmentprice));
		investment.setInvsetmentContent(investmentconcent);

		// 因为时间传回来世字符状态，所以要转化为date类型
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(1);
		Date date = null;
		try {
			date = dateFormat.parse(investmenttime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		investment.setInvestmentTime(date);
		investmentMapper.insert(investment);

		return "redirect:/investment";
	}

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

	// 初始显示全部数据，查询
	@GetMapping("/investment")
	public String investment(Model model, HttpSession session, String investmentid) {
		if (investmentid == null) {
			List<Investment> investment = investmentMapper.selectByExample(null);
			model.addAttribute("investment", investment);
		} else {
			int str = Integer.parseInt(investmentid);
			Investment investment = investmentMapper.selectByPrimaryKey(str);
			model.addAttribute("investment", investment);
		}

		return "investment"; 
	}

	// 初始显示全部数据，查询
	@ResponseBody
	@GetMapping("/find/{investmentid}")
	public Investment investment(HttpSession session, @PathVariable String investmentid) {
		if (investmentid == null) {
			return new Investment();
		} else {
			int str = Integer.parseInt(investmentid);
			Investment investment = investmentMapper.selectByPrimaryKey(str);
			return investment;
		}
	}

	// 删除
	@PostMapping("/deleteinvestment")
	public String deleteAdd(Model model, HttpSession session, String investmentid) {
		if (investmentid != null) {
			int id = Integer.parseInt(investmentid);
			investmentMapper.deleteByPrimaryKey(id);
		}

		return "redirect:/investment";

	}

	/*
	 * @PostMapping("/investment") public String search(Model model,HttpSession
	 * session, String msg ) { int str = Integer.parseInt(msg); Investment
	 * investment = investmentMapper.selectByPrimaryKey(str);
	 * 
	 * return "investment"; }
	 */

	@PostMapping("/updateinvestment")
	public String updateinvestment(Model model, HttpSession session, String investmentid
			,String investmenttitle,String investmenttime,String investmentprice,String invsetmentcontent) {
		
	int id = Integer.parseInt(investmentid);
	Investment invest = investmentMapper.selectByPrimaryKey(id);

	invest.setInvestmentTitle(investmenttitle);
	
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = dateFormat.parse(investmenttime);
		} catch (ParseException e) { // TODO Auto-generated catch block e.printStackTrace();
		}
		invest.setInvestmentTime(date);
		 	
	
	int price = Integer.parseInt(investmentprice);
	invest.setInvestmentPrice(price);
	invest.setInvsetmentContent(invsetmentcontent);

	System.out.println(investmentid);
	System.out.println(investmentprice);
	System.err.println(investmenttitle);
	investmentMapper.updateByPrimaryKeySelective(invest);
	
	return "redirect:/investment";
}

}
