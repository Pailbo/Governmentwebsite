package webadv.s16202126.Governmentweb.controller;

import java.util.Date;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import webadv.s16202126.Governmentweb.entity.Message;
import webadv.s16202126.Governmentweb.repository.Messagerepository;
@Controller
public class AdviceController {
	@Autowired
	Messagerepository messageMapper;

	@PostMapping("/advicepage")
	public String AdviceAdd(Model model, HttpSession session, String adviceContent) {
		Message message = new Message();
		message.setMessageContent(adviceContent);
		message.setMessageTime(new Date());
		messageMapper.insert(message);
		return "redirect:/advicepage";
	}

	@GetMapping("/advicepage")
	public String advicepage(Model model) {
		List<Message> messages = messageMapper.selectByExample(null);
		model.addAttribute("messages", messages);
		return "advicepage";
	}

	@PostMapping("/deleteAdviceForm")
	public String deleteAdd(Model model, HttpSession session, String feedbackId) {
		int id = Integer.valueOf(feedbackId);
		messageMapper.deleteByPrimaryKey(id);
		return "redirect:/advicepage";
	}
}
