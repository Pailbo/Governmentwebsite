package webadv.s16202126.Governmentweb.controller;

import java.util.Date;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import webadv.s16202126.Governmentweb.entity.Vidio;
import webadv.s16202126.Governmentweb.repository.Vidiorepository;

@Controller
public class VideoController {
	@Autowired
	Vidiorepository videoMapper;

	@PostMapping("/video")
	public String NewsAdd(Model model, HttpSession session, String videoUrl, String title) {
		Vidio vidio = new Vidio();
		vidio.setVidioLink(videoUrl);
		vidio.setVidioName(title);
		vidio.setVidioTime(new Date());
		videoMapper.insert(vidio);
		return "redirect:/video";
	}

	@GetMapping("/video")
	public String video(Model model) {
		List<Vidio> video = videoMapper.selectByExample(null);
		model.addAttribute("video", video);
		return "video";
	}

	@PostMapping("/deleteVideoFrom")
	public String deleteAdd(Model model, HttpSession session, String vidioId) {
		int id = Integer.valueOf(vidioId);
		videoMapper.deleteByPrimaryKey(id);
		return "redirect:/video";
	}
}
