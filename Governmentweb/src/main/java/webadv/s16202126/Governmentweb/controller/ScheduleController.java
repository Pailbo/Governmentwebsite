package webadv.s16202126.Governmentweb.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import webadv.s16202126.Governmentweb.entity.Investment;
import webadv.s16202126.Governmentweb.entity.Schedule;
import webadv.s16202126.Governmentweb.repository.Schedulerepository;

@Controller
public class ScheduleController {
	@Autowired

	Schedulerepository scheduleMapper;

	@PostMapping("/addschedule") 
	public String addnewinvestmnet(Model model, HttpSession session, int scheduleid,
			String scheduleperson, String scheduletime,String scheduleevent){
		
		Schedule schedule = new Schedule();
		schedule.setScheduleId(scheduleid);
		schedule.setSchedulePerson(scheduleperson);
		schedule.setScheduleEven(scheduleevent);
		
		  SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd"); 
		  Date date = new Date(); 
		  try { 
			  date = format.parse(scheduletime); 
			  } catch (ParseException e) {
				  
		  }
		  // TODO Auto-generated catch block e.printStackTrace(); }
		  schedule.setScheduleTiem(date);
		 
		scheduleMapper.insert(schedule);
				return "redirect:/schedule";
		
	}

	@GetMapping("/schedule")
	public String investment(Model model, HttpSession session, String scheduleid) {
		if (scheduleid == null) {
			List<Schedule> schedule = scheduleMapper.selectByExample(null);
			model.addAttribute("schedule", schedule);
		} else {
			int str = Integer.parseInt(scheduleid);
			Schedule schedule = scheduleMapper.selectByPrimaryKey(str);
			model.addAttribute("schedule", schedule);
		}

		return "schedule";
	}
	
	// ɾ��
		@PostMapping("/deleteschedule")
		public String deleteAdd(Model model, HttpSession session, String scheduleid) {
			if (scheduleid != null) {
				int id = Integer.parseInt(scheduleid);
				scheduleMapper.deleteByPrimaryKey(id);
			}

			return "redirect:/schedule";

		}
		
		@PostMapping("/updateschedule")
		public String updateinvestment(Model model, HttpSession session, String scheduleid
				,String scheduleperson,String scheduletime,String scheduleevent) {
			
		int id = Integer.parseInt(scheduleid);
		Schedule schedule = scheduleMapper.selectByPrimaryKey(id);

		schedule.setSchedulePerson(scheduleperson);
		
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			try {
				date = dateFormat.parse(scheduletime);
			} catch (ParseException e) { // TODO Auto-generated catch block e.printStackTrace();
			}
			schedule.setScheduleTiem(date);
			 	
		schedule.setScheduleEven(scheduleevent);
		scheduleMapper.updateByPrimaryKeySelective(schedule);
		
		return "redirect:/schedule";
	}
}
