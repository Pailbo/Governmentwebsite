package webadv.s16202126.Governmentweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import webadv.s16202126.Governmentweb.entity.Questionnaire;
import webadv.s16202126.Governmentweb.repository.Questionnairerepository;

@Controller
public class QuestionnaireController {

	@Autowired
	Questionnairerepository mapper;

	@PostMapping("/save")
	public String save(String optionsRadios1, String optionsRadios2, String optionsRadios3, String optionsRadios4, String optionsRadios5) {
		Questionnaire q = new Questionnaire();
		StringBuilder result = new StringBuilder(optionsRadios1).append("#")
				.append(optionsRadios2).append("#")
				.append(optionsRadios3).append("#")
				.append(optionsRadios4).append("#")
				.append(optionsRadios5);
		System.out.println(result);
		q.setQuestionnaireContent(result.toString());
		mapper.insert(q);
		return "redirect:home";
	}
}
