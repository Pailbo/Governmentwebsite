package webadv.s16202126.Governmentweb.controller;

import static org.hamcrest.CoreMatchers.nullValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties.Git;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import webadv.s16202126.Governmentweb.entity.Investment;
import webadv.s16202126.Governmentweb.entity.Policies;
import webadv.s16202126.Governmentweb.repository.Policiesrepository;

@Controller
public class PolicyController {
	@Autowired
	
	Policiesrepository policiesrepository;
	
	@PostMapping("/addpolicies")
	public String addpolicies(Model model,HttpSession httpSession, String policiesid,
			String policiestitle,String policiestime) {
		
		Policies policies = new Policies();
		policies.setPoliciesId(Integer.parseInt(policiesid));
		Date date = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-mm-dd");
		try {
			date = sFormat.parse(policiestime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		policies.setPoliciesTime(date);
		policies.setPoliciesTitle(policiestitle);
		policiesrepository.insert(policies);
		
		return "redirect:/policy";
		
	}
	
	@GetMapping("/policy")
	public String sreachpolicies(Model model,HttpSession httpSession,String policiesid) {
		if (policiesid == null) {
			List<Policies> policies = policiesrepository.selectByExample(null);
			model.addAttribute("policies", policies);
		} else {
			int str = Integer.parseInt(policiesid);
			Policies policies = policiesrepository.selectByPrimaryKey(str);
			model.addAttribute("policies", policies);
		}

		return "policy";
	}
	
	@GetMapping("/delectpolicies")
	public String delectpolicies(Model model,HttpSession httpSession,String policiesid) {
		if(policiesid!=null) {
			int policies = Integer.parseInt(policiesid);
			policiesrepository.deleteByPrimaryKey(policies);
		}
		System.out.println(policiesid);
	return "redirect:/policy";
	}
	
	@ResponseBody
	@GetMapping("/findpoliciesid/{policiesid}")
	public Policies find(HttpSession httpSession,@PathVariable String policiesid) {
		System.out.println(policiesid);
		if (policiesid == null) {
			System.out.println(11);
			return null;
		} else {
			int str = Integer.parseInt(policiesid);
			Policies policies = policiesrepository.selectByPrimaryKey(str);
			System.out.println(22);
			return policies;

		} 

	}
	
	@PostMapping("/updatepolicies")
	public String updateinvestment(Model model, HttpSession session, String policiesid
			,String policiestitle,String investmenttime) {
		
	int id = Integer.parseInt(policiesid);
	Policies policies = policiesrepository.selectByPrimaryKey(id);

	policies.setPoliciesTitle(policiestitle);


	policiesrepository.updateByPrimaryKeySelective(policies);
	
	return "redirect:/policy";
}
}
