
package webadv.s16202126.Governmentweb.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import webadv.s16202126.Governmentweb.entity.Questionnaire;
import webadv.s16202126.Governmentweb.entity.User;
import webadv.s16202126.Governmentweb.repository.Questionnairerepository;
import webadv.s16202126.Governmentweb.repository.Userrepository;


@Controller
public class HomeController {
	
	@PostMapping("/login")
	public String login(Model model,HttpSession session, String account, String password) {
		User user = userMapper.selectByPrimaryKey(account);
		
		 if (user != null && user.getUserPassword().equals(password)) {
				session.setAttribute("user", user);
				session.setAttribute("msg", null);
				
				return "home";
		 
			}else if(user==null){
				
				session.setAttribute("msg", "�˺Ŵ���");
	             
				 return "login";
			}
			else {
				session.setAttribute("msg", "�������");
				return "login";
			}
		
		
		
	} 
 
	@Autowired
	Userrepository userMapper;
	
	@Autowired
	Questionnairerepository questionaireMapper;

	@ResponseBody
	@GetMapping("/test")
	public List<Questionnaire> test() {
		return  questionaireMapper.selectByExample(null);
	} 
	
	@GetMapping("/")
	public String index(HttpSession session) {
		session.setAttribute("msg", null);
		return "login";
	}
	
	@GetMapping("/localstyle")
	public String localstyle() {
		return "localstyle";
	}

	
 
	@GetMapping("/home")
	public String home() {
		return "home";
	}
	
	@GetMapping("/surveypage")
	public String survey(Model model){
	
		List<Questionnaire> list= questionaireMapper.selectByExample(null);
		
		
		model.addAttribute("list", list);
	
		return "surveyPage";
		// 
	}


	

}
