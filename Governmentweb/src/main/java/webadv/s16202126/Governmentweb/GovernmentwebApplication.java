package webadv.s16202126.Governmentweb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@MapperScan("webadv.s16202126.Governmentweb.repository")
@SpringBootApplication
@ComponentScan
public class GovernmentwebApplication {

	public static void main(String[] args) {
		SpringApplication.run(GovernmentwebApplication.class, args);
	}

}
