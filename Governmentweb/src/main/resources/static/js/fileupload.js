function upload_init() {
    let mydropbox = document.getElementById('dropbox');
    mydropbox.ondragover = function (e) {
        // 阻止默认行为
        e.preventDefault();
        mydropbox.background = 'red';
    };
    mydropbox.ondragleave = function (e) {
        e.preventDefault();
        mydropbox.background = 'gray'
    };
    mydropbox.ondrop = function (e) {
        e.preventDefault();
        uploadFiles(e.dataTransfer.files);
    };
    let fileitems = document.getElementsByClassName('fileitem');
    for (let i = 0; i < fileitems.length; i++) {
        fileitems[i].onclick = function (e) {
            console.log(i, fileitems);
            let filePath = this.getAttribute("file-path");
            getFile(filePath);
        }
    }
    let copyBtns = document.getElementsByClassName('copy-btn');
    for (let i = 0; i < copyBtns.length; i++) {
        copyBtns[i].onclick = function (e) {
            let filePath = this.getAttribute("file-path");
            copy_filelink(filePath);
            e.stopPropagation();
        }
    }
};

function createXMLHttpRequest() {
    try {
        return new XMLHttpRequest();
    } catch (e) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert("xxx");
                throw e;
            }
        }
    }
}

function select_file() {
    var inputObj = document.createElement('input')
    inputObj.setAttribute('type', 'file');
    inputObj.setAttribute("style", 'visibility:hidden');
    inputObj.setAttribute("multiple", 'multiple');
    document.body.appendChild(inputObj);
    inputObj.click();
    inputObj.onchange = function (e) {
        uploadFiles(inputObj.files);
    };
}

function uploadFiles(files) {
    let mydropbox = document.getElementById('dropbox');
    let msg = document.getElementById('msg');
    mydropbox.background = 'gray';
    let frag = document.createDocumentFragment();
    let count = 1;
    for (let i = 0; i < files.length; i++) {
        let file = files[i];
        if (msg !== undefined) {
            msg.style.display = 'none';
        }
        let fileItemDiv = document.createElement('div');
        let sizeDiv = document.createElement('div');
        let nameDiv = document.createElement('div');
        let msgDiv = document.createElement('div');
        fileItemDiv.setAttribute('class', 'fileitem');
        sizeDiv.setAttribute('class', 'sizeDetail');
        nameDiv.setAttribute('class', 'nameDetail');
        msgDiv.setAttribute('class', 'msgDetail');
        fileItemDiv.appendChild(sizeDiv);
        fileItemDiv.appendChild(nameDiv);
        fileItemDiv.appendChild(msgDiv);
        frag.appendChild(fileItemDiv);
        sizeDiv.innerHTML = '<span><strong>' + file.size + 'B</strong></span>';
        console.log(file, file.size);
        nameDiv.innerHTML =
            '<span>' +
            (file.name.length > 6 ? file.name.slice(0, 6) + '...' : file.name) +
            '</span>';
        // K M
        if (file.size > 10 * 1024 * 1024) {
            msgDiv.innerHTML = '<span style="color: red">不能超过10MiB</span>';
        } else {
            msgDiv.innerHTML = '<span>上传中</span>';
            let fd = new FormData();
            fd.append('file', file);
            let xhr = new createXMLHttpRequest();
            xhr.open('post', getContextPath() + '/attach', true);
            xhr.countI = count;
            msgDiv.countI = count++;
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        msgDiv.innerHTML = '<span style="color: green">上传成功</span>';
                    } else {
                        msgDiv.innerHTML = '<span style="color: red">上传失败' + xhr.status + '</span>';
                    }
                }
            };
            xhr.send(fd);
        }
    }
    document.getElementById('files').appendChild(frag);
}

function copy_filelink(link) {
    copyToClipboard(document.location.origin + link);
    return false;
}

function getFile(link) {
    document.location = link;
}